# blog
## 博客原型参考
    老头子Blogs http://mazhenxin.xyz
    CodeSheep程序羊 https://www.codesheep.cn

## 功能概述
    博客的发布 查看 评论 分类 标签

## 前端技术: vue element-ui 
    具体涉及到:
        axios router vuex flex布局 
        mavon-editor markdown-it github-markdown-css

## 参考文档/资源:
    * flex: https://www.ruanyifeng.com/blog/2015/07/flex-grammar.html
    * 我的笔记: https://shimo.im/docs/rHYVwTp9xP9KWPYp
    * 视频教程:
        b站最火vue: https://www.bilibili.com/video/BV15741177Eh
        Markhub视频教程: https://www.bilibili.com/video/BV1PQ4y1P7hZ
        vue-cli使用: https://www.bilibili.com/video/BV1oV411o71h

## 具体需求描述
    ### Home页面
        首部显示一句名言警句, 展现个人价值观 [路漫漫其修远兮 吾将上下而求索]
        左部时间轴串联文章历史
        Home只显示六条文章
            每一个文章Card显示有: title, [] tags, author, create_time, picture
        右侧显示简介(昵称, 技术栈, 兴趣, 文章数, 便签)
    ### 分类页面
        一句quote开头
        下方flex布局,响应式显示
            每一个Card显示有: className, picture, classDes
    ### 发布文章
        重置按钮点击后,仅仅将内容清空
        发布前检空
        发布文章点击发布后,弹出对话框,效果类似csdn
    ### 文章展示页面
        参考老头子Blogs的效果
        创建时间 作者 访问量
        评论功能
    ### 归档
        历史线 带分页展示功能 
        右侧显示年份线

## 亟待解决的bug

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
