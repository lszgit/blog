import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import mavonEditor from 'mavon-editor'

import VueAxios from 'vue-axios'
import Element from 'element-ui'
import App from '@/App'
import router from '@/router'
import store from '@/store'

import 'element-ui/lib/theme-chalk/index.css'
import 'mavon-editor/dist/css/index.css'

Vue.use(Element)
Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(mavonEditor)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
