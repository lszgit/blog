import request from '@/utils/request'

export function fetchLinkList() {
  return request({
    url: '/links',
    method: 'get',
    params: { 'page': 1, 'page_size': 50 }
  })
}
