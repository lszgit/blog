import request from '@/utils/request'

export function fetchAuthor() {
  return request({
    url: '/author',
    method: 'get'
  })
}
