import request from '@/utils/request'

export function fetchCategory(page, pageSize) {
  return request({
    url: `/categories`,
    method: 'get',
    params: { 'page': page, 'page_size': pageSize }
  })
}
