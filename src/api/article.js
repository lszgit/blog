import request from '@/utils/request'

export function fetchList(page, pageSize) {
  return request({
    url: '/articles',
    method: 'get',
    params: {
      page: page,
      page_size: pageSize
    }
  })
}

export function GetFirstPageList(page, pageSize) {
  return request({
    url: '/articles/first',
    method: 'get',
    params: {
      page: page,
      page_size: pageSize
    }
  })
}

export function getArticleByID(articleID) {
  return request({
    url: `/articles/${articleID}`,
    method: 'get'
  })
}

export function getAmount() {
  return request({
    url: `/articles/tags/amount`,
    method: 'get'
  })
}

export function fetchClassList(category_id, page, pageSize) {
  return request({
    url: `/articles/category/${category_id}`,
    method: 'get',
    params: {
      page: page,
      page_size: pageSize
    }
  })
}
