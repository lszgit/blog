import axios from 'axios'
import { Message } from 'element-ui'

const baseUrl = 'http://127.0.0.1:8972/blogApi/v1/'
// const baseUrl = 'http://localhost:8972/blogApi/v1'

// create an axios instance
const service = axios.create({
  baseURL: baseUrl,
  timeout: 5000
})

// request interceptor
service.interceptors.request.use(
  config => {
    /*
        if (store.getters.token) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            // config.headers['token'] = getToken()
        }
        */
    return config
  },
  error => {
    // console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.code !== 0) {
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    // console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
