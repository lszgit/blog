import Vue from 'vue'
import Home from '@/views/home'
import Category from '@/views/category'
import Archive from '@/views/archive'
import Login from '@/views/login'
import Layout from '@/layout'
import Article from '@/views/article'
import ClassList from '@/views/category_article'
import FriendChain from '@/views/friend_link'

import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'layout',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'home',
        component: Home,
        meta: { title: '主页' }
      },
      {
        path: '/category',
        name: 'category',
        component: Category,
        meta: { title: '分类' }
      },
      {
        path: '/archive',
        name: 'archive',
        component: Archive,
        meta: { title: '归档' }
      },
      {
        path: '/login',
        name: 'login',
        component: Login,
        meta: { title: '登录' }
      },
      {
        path: '/article/:articleId',
        name: 'blogDetail',
        component: Article,
        meta: { title: '内容' }
      },
      {
        path: '/classify/:classId',
        name: 'ClassList',
        component: ClassList,
        meta: { title: '分类' }

      },
      {
        path: '/friends',
        name: 'FriendChain',
        component: FriendChain,
        meta: { title: '友链' }
      }
    ]
  }
]

const router = new VueRouter({
  // mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

export default router
